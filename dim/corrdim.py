import numpy as np
import numpy.linalg

class CorrelationDimension(object):
    def __init__(self, data):
        self.data_distance = []
        for i, di in enumerate(data):
            for dj in data[(i+1):len(data)]:
                self.data_distance.append(numpy.linalg.norm(di-dj))
        self.data_distance.sort()

    def C2(self, epsilon):
        i = None
        for ii, dd in enumerate(self.data_distance):
            if dd > epsilon:
                i = ii
                break
        if i is None:
            i = len(self.data_distance)
        elif i == 0:
            i = 1
        return float(i)/float(len(self.data_distance))

    def dcor(self, epsilon1, epsilon2):
        if np.log(epsilon2) == np.log(epsilon1):
            return 0
        else:
            return (np.log(self.C2(epsilon2)) - np.log(self.C2(epsilon1))) \
                    / (np.log(epsilon2) - np.log(epsilon1))

    def dcor_log(self, log_epsilon1, log_epsilon2):
        if log_epsilon1 == log_epsilon2:
            #print "e1 == e2"
            #assert False
            return 0.
        else:
            log_C2_2 = np.log(self.C2(np.exp(log_epsilon2)))
            log_C2_1 = np.log(self.C2(np.exp(log_epsilon1)))
            if log_C2_2 == log_C2_1:
                #print "log_C2_2 == log_C2_1", log_C2_2, log_C2_1
                #assert False
                return 0.
            return (log_C2_2 - log_C2_1) / (log_epsilon2 - log_epsilon1)
