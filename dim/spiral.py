import numpy as np
# import matplotlib.pylab as plt


class CartesianFactor(object):
    def __init__(self, factors=None):
        self.define_parameters(factors)

    def define_parameters(self, factors=None):
        self.factors = factors

    def cloud(self, points=1000, seed=None):
        self.data = self.gen_data(points=points, seed=seed)

    def gen_data(self, points=1000, seed=None):
        di = self.factors[0].gen_data(points)
        for i in range(1,len(self.factors)):
            di = np.hstack([di, self.factors[i].gen_data(points)])
        return di

    def intrinsic(self):
        return sum([f.intrinsic() for f in self.factors])

    def embedding(self):
        return sum([f.embedding() for f in self.factors])

    def __mul__(self, other):
        return CartesianFactor(self.factors+other.factors)

    def __rmul__(self, other):
        return CartesianFactor(other.factors+self.factors)

    def __pow__(self, power):
        return CartesianFactor(self.factors*power)


class Spiral(CartesianFactor):
    def define_parameters(self, factors=None):
        self.turns = 5.
        self.factors = (self,)

    def gen_data(self, points=1000, seed=None):
        np.random.seed(seed)
        x = np.random.uniform(size=points)
        phases = self.turns*2*np.pi*np.sqrt(x)
        d0 = np.sqrt(x)*np.cos(phases)
        d1 = np.sqrt(x)*np.sin(phases)
        return np.vstack([d0, d1]).transpose()

    def intrinsic(self):
        return 1

    def embedding(self):
        return 2


class LineSegment(CartesianFactor):
    def define_parameters(self, factors=None):
        self.length = 1.
        self.factors = (self,)

    def gen_data(self, points=1000, seed=None):
        np.random.seed(seed)
        d0 = np.random.uniform(size=points)
        return np.vstack([d0]).transpose()

    def intrinsic(self):
        return 1

    def embedding(self):
        return 1


class Circle(CartesianFactor):
    def define_parameters(self, factors=None):
        self.radius = 1.
        self.factors = (self,)

    def gen_data(self, points=1000, seed=None):
        np.random.seed(seed)
        phases = 2.*np.pi*np.random.uniform(size=points)
        d0 = self.radius*np.cos(phases)
        d1 = self.radius*np.sin(phases)
        return np.vstack([d0, d1]).transpose()

    def intrinsic(self):
        return 1

    def embedding(self):
        return 2


class Helix(CartesianFactor):
    def define_parameters(self, factors=None):
        self.radius = 1.
        self.turns = 5.
        self.dz = 1.
        self.factors = (self,)

    def gen_data(self, points=1000, seed=None):
        np.random.seed(seed)
        phases01 = self.turns*np.random.uniform(size=points)
        d0 = self.radius*np.cos(2*np.pi*phases01)
        d1 = self.radius*np.sin(2*np.pi*phases01)
        d2 = self.dz*phases01
        return np.vstack([d0, d1, d2]).transpose()

    def intrinsic(self):
        return 1

    def embedding(self):
        return 3


class Point(CartesianFactor):
    def define_parameters(self, factors=None):
        self.factors = (self,)

    def gen_data(self, points=1000, seed=None):
        np.random.seed(seed)
        d0 = np.zeros([1, points])
        return np.vstack([d0]).transpose()

    def intrinsic(self):
        return 0

    def embedding(self):
        return 1


if __name__ == '__main__':
    S = Spiral()
    L = LineSegment()
    C = Circle()
    H = Helix()
    P = Point()
    M = (S**3)*(L**4)*(P**5)

    # seed=
    #     n0 = rstate.normal(scale=noise, size=points)
    #     n1 = rstate.normal(scale=noise, size=points)
