import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from matplotlib.colors import BoundaryNorm

def visualize(CD, res):
    mm = np.log(CD.data_distance[0])
    MM = np.log(CD.data_distance[-1])
    dx = (MM-mm)/res
    y, x = np.mgrid[slice(mm, MM + dx, dx),
                    slice(mm, MM + dx, dx)]
    rng = [x[i,i] for i in range(len(x))]
    z = np.zeros([len(y), len(x)])
    for i, xi in enumerate(rng):
        for j, yj in enumerate(rng[0:i]):
            zij = CD.dcor_log(xi,yj)
            z[i,j] = zij
            z[j,i] = zij
            if i != j and zij == 0.:
                print "Zero dimension found at i,j = ", i, j
    z = z[:-1, :-1]
    levels = MaxNLocator(nbins=15).tick_values(z.min(), z.max())
    cmap = plt.get_cmap('YlOrRd')
    norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
    fig, ax = plt.subplots()
    im = ax.pcolormesh(x, y, z, cmap=cmap, norm=norm)
    fig.colorbar(im, ax=ax)
    ax.set_title('Scale Dependent Correlation Dimension')
